# Generated by Django 2.1.1 on 2018-09-07 11:53

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('movie', '0007_auto_20180904_1216'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='review',
            field=models.TextField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='movie',
            name='cast',
            field=models.CharField(max_length=90),
        ),
        migrations.AlterField(
            model_name='movie',
            name='director',
            field=models.CharField(max_length=90),
        ),
        migrations.AlterField(
            model_name='movie',
            name='short_desc',
            field=models.CharField(max_length=240),
        ),
        migrations.AlterField(
            model_name='movie',
            name='title',
            field=models.CharField(max_length=75),
        ),
        migrations.AlterField(
            model_name='movie',
            name='writer',
            field=models.CharField(max_length=90),
        ),
    ]
