from django.utils.timezone import datetime
from django.db.models import Q
from movie.models import Category, Contact, Movie
from django_filters import rest_framework as filters 
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView

from .serializer import (ContactCreateSerializer, ContactListSerializer,
                         HomeSerializer, HomeTestSerializer,
                         MovieDetailSerializer, MovieListSerializer)

from .pagination import PostPageNumberPagination


class MovieListAPIView(ListAPIView):
    serializer_class = MovieListSerializer
    pagination_class = PostPageNumberPagination

    def get_queryset(self, *args, **kwargs):
        queryset_list = Movie.objects.all()
        query = self.request.GET.get("q")
        query_month = self.request.GET.get("m")
        query_filter =self.request.GET.get("f")
        if query:
            queryset_list = queryset_list.filter(
                Q(title__icontains=query)|
                Q(release__year=query)
                
            ).distinct()
        
        if query_month:
            queryset_list = queryset_list.filter(
                Q(release__month=query_month) #month query make seperte             
            ).distinct()
        
        if query_filter:
            queryset_list = queryset_list.filter(
                Q(category = query_filter)|
                Q(release__year=query_filter)
            ).distinct()
        return queryset_list
# #



class ContactListAPIView(ListAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactListSerializer 


class ContactCreateAPIView(CreateAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactListSerializer 




class MovieDetailAPIView(RetrieveAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieDetailSerializer


class HomeAPIView(ListAPIView):
    serializer_class = HomeSerializer

    def get_queryset(self, *args, **kwargs):
        queryset_list = Movie.objects.all()
        query_month = self.request.GET.get("m")
        if query_month:
            queryset_list = queryset_list.filter(
                Q(release__month=query_month) #month query make seperte             
            ).distinct()
            return queryset_list[:4]
        queryset_list = Movie.objects.order_by("-release")[:6]

        return queryset_list

# class HomeTestAPIView(ListAPIView):
#     # queryset = Movie.objects.all()
#     # serializer_class = HomeTestSerializer

#     today = datetime.today()
#     print(today)
