from django.contrib import admin
from django.urls import path
from django.conf.urls import url

from .views import (
    MovieListAPIView,
    MovieDetailAPIView,
    ContactCreateAPIView,
    ContactListAPIView,
    HomeAPIView,
)

urlpatterns = [
    url(r'^movie-list/$',MovieListAPIView.as_view() ,name = 'list'),
    url(r'^movie/(?P<pk>\d+)/$',MovieDetailAPIView.as_view() ,name = 'detail'),
    url(r'^contact-list/$',ContactListAPIView.as_view() ,name = 'contact_list'),
    url(r'^contact/$',ContactCreateAPIView.as_view() ,name = 'contact'),
    url(r'^home/$',HomeAPIView.as_view() ,name = 'home'),

]
