import django_filters
from movie.models import Category, Contact, Movie


class MonthFilter(django_filters.FilterSet):
    
    get_month = django_filters.CharFilter(lookup_expr='iexact')

    class Meta:
        model = Movie
        fields = {
            'release__month': ['exact'],
        }
    pass

class YearFilter(django_filters.FilterSet):
    
    release_year = django_filters.NumberFilter(field_name='release', lookup_expr='year')

    class Meta:
        model = Movie
       
