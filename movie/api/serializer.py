from rest_framework import serializers
from movie.models import Movie, Category, Contact


class MovieListSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Movie
        fields = (
            'id',
            'title', 
            'short_desc', 
            'rating', 
            'runtime',
            'release',
            'get_category',
            'director',
            'writer',
            'cast',
            'image',
            'created',
            'get_month',
            )


class MovieDetailSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Movie
        fields = (
            'id',
            'title', 
            'short_desc', 
            'rating', 
            'runtime',
            'release',
            'get_category',
            'director',
            'writer',
            'cast',
            'review',
            'image',
            )

class ContactListSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Contact
        fields = (
            'id',
            'name',
            'email',
            'website',
            'message',
            'created',
            )

class ContactCreateSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Contact
        fields = (
            'name',
            'email',
            'website',
            'message',
            )


class HomeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie
        fields = (
            'id',
            'title',
            'release',
            'image',
        )



class HomeTestSerializer(serializers.Serializer):
    month = serializers.DateTimeField('%b')
    movies = serializers.StringRelatedField(many=True)
   

    class Meta:
        model = Movie
        fields = (
            'month',
            'movies',
        )
