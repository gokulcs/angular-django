from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Categories"

class Movie(models.Model):
    title = models.CharField(max_length=75)
    short_desc = models.CharField(max_length=240)
    rating = models.IntegerField(blank = True)
    runtime = models.IntegerField(blank = True)
    release = models.DateField()
    category = models.ManyToManyField(Category, related_name = "category_tag")
    director = models.CharField(max_length=90)
    writer = models.CharField(max_length=90)
    cast = models.CharField(max_length=90)
    review = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(null=True, blank=True, upload_to='posters/')

    class Meta:
        ordering = ['title']
   
    def get_category(self):
        
        movie = Movie.objects.filter(id=self.id)
        cat_qs = Category.objects.filter(category_tag__id=self.id)
        cat = ""
        for item in cat_qs:            
            cat += item.name + '/'         
        
        return cat


    def get_month(self):
        print("\n\n\n")
        month = Movie.objects.get(id=self.id)
        release = month.release
        print(month.release)
        month = release.strftime('%B')
        print("\n\n\n")
        return str(month)


    def __str__(self):
        return self.title
    
   

class Contact(models.Model):
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=150)
    website = models.CharField(max_length=90, blank = True)
    message = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name 

