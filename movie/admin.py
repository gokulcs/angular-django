from django.contrib import admin
from movie.models import Movie, Category, Contact

# Register your models here.

class MovieAdmin(admin.ModelAdmin):

    list_display = (
            'title', 
            'short_desc', 
            'rating', 
            'runtime',
            'release',
            'get_category',
            'director',
            'writer',
            'cast',
            )

class CategoryAdmin(admin.ModelAdmin):

    list_display = (
            'name', 
            )


class ContactAdmin(admin.ModelAdmin):

    list_display = list_display = (
            'name', 
            'email', 
            'website', 
            'message',
            )




admin.site.register(Movie, MovieAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Contact, ContactAdmin)